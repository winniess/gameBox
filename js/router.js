//路由构造器
function Router() {
    //接受所有的配置路由内容：锚 和 函数方法
    this.routes = {};
    //接受 改变后的 hash值
    this.curUrl = '';
    //将定义的所有路由和函数方法 传给 routes
    this.route = function (path, callback) {
        this.routes[path] = callback || function () {};
        // console.log('routes[path]:'+this.routes[path])
    };
    //hash 值改变时触发的 函数方法
    this.refresh = function () {
        //获取改变的hash值（url中锚 # 号后面的文本）
        this.curUrl = location.hash.slice(1) || '/';
        this.routes[this.curUrl]();
        // console.log('location.hash:'+location.hash);
        // console.log('curUrl:'+this.curUrl);
        // console.log('this.routes[this.curUrl]:'+this.routes[this.curUrl])
    };
    //监听load（加载url）、hashchange（hash改变）事件
    this.init = function () {
        window.addEventListener('load',this.refresh.bind(this),false);
        window.addEventListener('hashchange',this.refresh.bind(this),false)
    }
}
var R = new Router();//使用Router构造器
R.init();//监听时间
var res = document.getElementById('content');//读取id为result的元素
//定义所有需要用的 路由：hash值 和 加载的内容
R.route('/', function () {
    $(".footer li").removeClass("active").eq(0).addClass("active");
    $("#content").load("html/index.html",null,function(){
        $(".gameList ul::-webkit-scrollbar").css("display","none");
    });
})
R.route('/list', function () {
    $("#content").load("html/list.html");
    $(".footer li").removeClass("active").eq(1).addClass("active");
    
})
R.route('/my', function () {
    $(".footer li").removeClass("active").eq(2).addClass("active");
    $("#content").load("html/my.html");
    setTimeout(() => {
        var isWeixin = GameCenter.checkIsWeixin();
        if(isWeixin){
            $("#buttonLog").hide();
        }else{
            if(true){//已经登录显示'退出'
                $("#buttonLog").text("退出").show();
            }else{
                $("#buttonLog").text("登录").show();
            }
        }
    }, 0);
    
})
R.route('/agree', function () {
    $(".footer li").removeClass("active").eq(2).addClass("active");
    // $("#content").load("https://3g.tuyoo.com/protocol2.html");
    $("#content").load("html/agree.html");
})
R.route('/fcm', function () {
    $(".footer li").removeClass("active").eq(2).addClass("active");
    $("#content").load("html/fcm.html");
})
initWindow(window, 750);



