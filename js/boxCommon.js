/**
 * 一些通用方法
 */
(function (exports) {
    /**
     * 
     * @param {string} dom 目标dom元素
     * @param {string} className  效果的class
     */
    exports.tabEffect = function (dom, className) {
        // if (typeof dom === 'string') {
        //     dom = document.querySelector(dom);
        // }
        $("body").on("click", dom, function () {
            var index = $(this).index();
            $(dom).removeClass(className).eq(index).addClass(className);
        })

    }
      /**
     * 将对象渲染到模板
     * @param {String} template 对应的目标
     * @param {Object} obj 目标对象
     * @return {String} 渲染后的模板
     */
    exports.renderTemplate = function(template, obj) {
        return template.replace(/[{]{2}([^}]+)[}]{2}/g, function($0, $1) {
            return obj[$1] || '';
        });
    };

    /**
     * 定义一个计数器
     */
    var counterArr = [0];

    /**
     * 添加测试数据
     * @param {String} dom 目标dom
     * @param {Number} count 需要添加的数量
     * @param {Boolean} isReset 是否需要重置，下拉刷新的时候需要
     * @param {Number} index 属于哪一个刷新
     */
    exports.appendTestData = function (dom, count, isReset, index) {
        if (typeof dom === 'string') {
            dom = document.querySelector(dom);
        }

        var prevTitle = typeof index !== 'undefined' ? ('Tab' + index) : '';

        var counterIndex = index || 0;

        counterArr[counterIndex] = counterArr[counterIndex] || 0;

        if (isReset) {
            dom.innerHTML = '';
            counterArr[counterIndex] = 0;
        }

        var template = '<li class="list-item"><h3 class="msg-title">{{title}}</h3><span class="msg-fs14 msg-date">{{date}}</span></li>';

        var html = '',
            dateStr = (new Date()).toLocaleString();

        for (var i = 0; i < count; i++) {
            html += exports.renderTemplate(template, {
                title: prevTitle + '测试第【' + counterArr[counterIndex] + '】条新闻标题',
                date: dateStr
            });

            counterArr[counterIndex]++;
        }

        var child = exports.parseHtml(html);

        dom.appendChild(child);
    };

    /**
     * 绑定监听事件 暂时先用click
     * @param {String} dom 单个dom,或者selector
     * @param {Function} callback 回调函数
     * @param {String} eventName 事件名
     */
    exports.bindEvent = function (dom, callback, eventName) {
        eventName = eventName || 'click';
        if (typeof dom === 'string') {
            // 选择
            dom = document.querySelectorAll(dom);
        }
        if (!dom) {
            return;
        }
        if (dom.length > 0) {
            for (var i = 0, len = dom.length; i < len; i++) {
                dom[i].addEventListener(eventName, callback);
            }
        } else {
            dom.addEventListener(eventName, callback);
        }
    };
    /**
	     * @return {boolean}
	     */
    exports.IsNull=function (v) {
        return v === undefined || v === null || v === "";
    };
    /**
 *
 *
 * @param {*} url 请求url
 * @param {*} requestData  请求参数
 * @param {*} callback 回调函数
 */
    exports.getData= function (url, requestData, callback) {
    $.get(url, requestData, function (data) {
         if(data.retcode == 0){
             alert("shibai");
         }else{
             callback(data);
         }
    }, "json");

}

exports.authUrl = function(gameId) {
    window.location.href="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe88e63e92a2273d8&redirect_uri="+encodeURIComponent("http://h5game.tuyoo.com/?act=game.init&game="+gameId)+"&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect";
    // window.location.href="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxe88e63e92a2273d8&redirect_uri="+encodeURI("http://h5game.tuyoo.com/?act=game.init&game="+gameId)+"&response_type=code&scope=snsapi_userinfo&state=123#wechat_redirect";
};


    exports.checkIsWeixin = function(){
        if(!window.nurse) {
            window.nurse = new Object;
        }
        var u = navigator.userAgent,
            app = navigator.appVersion;
        var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
        var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        var  isWeixin = !!u.match(/MicroMessenger/i);//weixin
        if(isWeixin){
            return true;
        }
        return false;
    }

})(window.GameCenter = {});