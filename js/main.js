
var listDomArr = [],
    requestDelayTime = 600,
    miniRefreshArr = [],
    pageArr=[],
    currIndex = 0;
    
var dom = {
    tab: 1,
    pageL: 1,
    pageR: 1,
    initSwiper: function (className, activeClass) {
        var navControl = document.querySelector('.top_tab');
        var mySwiper = new Swiper('#center_main', {
            autoplay: 0,
            observer: true,//修改swiper自己或子元素时，自动初始化swiper 
            observeParents: true,//修改swiper的父元素时，自动初始化swiper 
            onSlideChangeEnd: function (swiper) {
                var index = swiper.activeIndex;
                navControl.querySelector('.active').classList.remove('active');
                navControl.children[index].classList.add('active');
                currIndex = index;
            }
        });
        var bannerSwiper = new Swiper('.bannerBox', {
            autoplay: 2000,
            loop: true,
            pagination: '.swiper-pagination'
        });
        $("#content").on("click", ".top_tab li", function () {
            var index = $(this).index();
            $(".top_tab li").removeClass("active").eq(index).addClass("active");
            dom.tab = Number(index + 1);
            mySwiper.slideTo(index, 500);
        })
    },
    initMiniRefreshs: function (index) {
        listDomArr[index] = document.querySelector('#listdata' + index);
        miniRefreshArr[index] = new MiniRefresh({
            container: '#minirefresh' + index,
            down: {
                isAuto: true,
                callback: function () {
                    doAjax('down', index);
                }
            },
            up: {
                isAuto: true,
                callback: function () {
                    doAjax('up', index);
                    // setTimeout(function () {
                    //     miniRefreshArr[index].endUpLoading(listDomArr[index].children.length >= maxDataSize ? true : false);
                    // }, requestDelayTime);
                }
            }
        });

    }
}
    
setTimeout(() => {
    initData();
}, 0);

function initData(){
    /**
     * 获取tab
     */
    GameCenter.getData("js/ajax_tabs.json",{},function(data){
        var tabList = data.data.tabs;
        var html = "";
        var activeShow="";
        for(let i =0,len=tabList.length;i<len;i++){
            pageArr[i]=0;
            dom.initMiniRefreshs(i);
            if(i == 0){
            activeShow="active";
            }else{
                activeShow="";
            }
            html +=  '<li class="'+activeShow+'"><div>'+tabList[i]['title']+'</div></li>';
        }
        $(".top_tab").html(html);
        dom.initSwiper();
    })
}

/**
 * 
 * @param {String} downOrUp  下拉刷新还是上蜡加载
 * @param {Number} index 当前tab的索引
 */
var totalPage = 1;
function doAjax(downOrUp, index) {
    var page = 0;
    if (downOrUp == 'down') {
        pageArr[index]=1;
    } else {
        pageArr[index]++;
    }
    page= pageArr[index];
    var tab = index - 0 + 1;
    console.log(totalPage);
    if(totalPage < page){
        miniRefreshArr[index].endUpLoading(true);// 结束上拉加载
        return;
    }
    $.ajax({
        // url: "js/ajax_list.json",
        url: "http://h5game.tuyoo.com/?act=game.getinfo",
        type: "get",
        data: {
            "tab_id": tab,
            "page": page,
            "psize": "10",
            't': new Date().getTime()
        },
        dataType: "json",
        success: function (data) {
            if (data.retCode == 1) {
                var dataT = data.data;
                if (dataT != null) {
                    var arrLen = dataT.list.length;
                    totalPage = dataT.totalPage;
                    if (arrLen > 0) {
                        var html = appendList(dataT.list);
                        // var html = '<li><a href=""><img class="imgUrl" src="" alt=""><div class="right"><h5 class="name">我是第二<img class="icon_hot" src="https://downqn.tuyoo.com/offical_accounts/gameBox/icon_tag@2x.png" /></h5><p class="des">游戏介绍多了十八个就隐藏就隐藏就隐藏就隐藏</span><p class="howmany"><em>122</em>人在玩</span></div><button class="goGame">进入</button></a></li>';
                        setTimeout(function () { // 使用 setTimeout 函数是方便演示的，你可以不用 setTimeout 函数
                            if (downOrUp == 'down') {
                                $('#listdata' + index).html(html);
                                miniRefreshArr[index].endDownLoading(true);// 结束下拉刷新
                            } else {
                                $('#listdata' + index).append(html);
                                if (totalPage == page) {// 是否已取完数据页
                                    miniRefreshArr[index].endUpLoading(true);// 结束上拉加载
                                } else {
                                    miniRefreshArr[index].endUpLoading(false);
                                }
                            }
                        }, requestDelayTime);
                        return false;
                    }
                }
            }
            if (downOrUp == 'down') {
                miniRefreshArr[index].endDownLoading(true);
            } else {
                miniRefreshArr[index].endUpLoading(true);
            }
        },
        error: function (e) {
            console.log(e);
            if (downOrUp == 'down') {
                miniRefreshArr[index].endDownLoading(true);
            } else {
                miniRefreshArr[index].endUpLoading(true);
            }
        }
    });
}

function appendList(data){
    var html = "";
    var len =data.length;
    var template = '<li>'+
        '<a href="javascript:GameCenter.authUrl({{gameId}});">'+
            '<img class="imgUrl" src="{{src}}" alt="">'+
            '<div class="right">'+
                '<h5 class="name">{{name}}<img class="icon_hot" style="{{display}" src="https://downqn.tuyoo.com/offical_accounts/gameBox/icon_tag@2x.png"/></h5>'+
                '<p class="des">{{description}}</span>'+
                    '<p class="howmany"><em>{{people}}</em>人在玩</span>'+
            '</div>'+
            '<button class="goGame">进入</button>'+
        '</a>'+
    '</li>';
    
    for (var i = 0; i < len; i++) {
        if(data[i].isHot == 1){
            var isShow = "display:inline";
        }else{
            var isShow = "display:none";
        }
        html += GameCenter.renderTemplate(template, {
            display:isShow,
            gameId:data[i].gameId,
            name: data[i].name,
			src: data[i].src,
            description: data[i].describe,
            peopleNum: data[i].people
        });
    }
    return html;
}